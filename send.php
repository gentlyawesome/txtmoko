<?php

//##########################################################################
// ITEXMO SEND SMS API - CURL METHOD
// Visit www.itexmo.com/developers.php for more info about this API
//##########################################################################
function itexmo($number,$message,$apicode){
			$ch = curl_init();
			$itexmo = array('1' => $number, '2' => $message, '3' => $apicode);
			curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			 curl_setopt($ch, CURLOPT_POSTFIELDS, 
			          http_build_query($itexmo));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			return curl_exec ($ch);
			curl_close ($ch);
}
function itexmo_bal($apicode){
			$ch = curl_init();
			$itexmo = array('4' => $apicode);
			curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			 curl_setopt($ch, CURLOPT_POSTFIELDS, 
			          http_build_query($itexmo));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			return curl_exec ($ch);
			curl_close ($ch);
}
//##########################################################################

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
@$clientNum = $request->clientNum;
@$clientMsg = $request->clientMsg;
$message = "";

$api_code = "gentlyawesome426TG";			
$result = itexmo("$clientNum","$clientMsg",$api_code);
if ($result == ""){
echo "iTexMo: No response from server!!! <br>
Please check the METHOD used (CURL or CURL-LESS). If you are using CURL then try CURL-LESS and vice versa.	
Please <a href=\"https://www.itexmo.com/contactus.php\">CONTACT US</a> for help. ";	
  $message = "Server is down :(";
}else if ($result == 0){
  $message = "Message Sent!";
}
else{	
//echo "Error Num ". $result . " was encountered!";
}

switch($result){
   case 0:
     $message = "Message Sent!";
     break;
   case 1:
     $message = "Invalid Number";
     break;
   case 2:
     $message = "Number Not Supported";
     break;
   case 4:
     $message = "Maximum Message per day reached. This will be reset every 12MN.";
     break;
   case 5:
     $message = "Maximum allowed characters for message reached.";
     break;
   case 6:
     $message = "System OFFLINE";
     break;
   case 7:
     $message = "Database Connection Error.";
     break;
   case 8:
     $message = "Database Error.";
     break;
   case 10:
     $message = "Recipient's number is blocked due to FLOODING, message was ignored.";
     break;
}
			
$result = itexmo_bal($api_code);
if ($result == "INVALID"){
//  echo "Invalid ApiCode";
}else{
//  echo "Your remaining balance for the days is " . $result;
}


$data = [ 
'result' => $result, 
'message' => $message
];

echo json_encode($data);


