(function(){
var app = angular.module('txtmoko', []);

app.controller("TxtmokoController", function($scope, $http){
  this.result = "Text";

  $http({
      method: "post",
      url: window.location.href + 'send.php',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
   }).success(function(data){
     $scope.messages_left = data;
   });
 

  this.sendMsg = function(){
    this.result = this.clientMsg;
 
    var request = $http({
      method: "post",
      url: window.location.href + 'send.php',
      data: {
        clientNum: this.clientNum,
        clientMsg: this.clientMsg
      }, 
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });

  
    request.success(function(data){
      $scope.messages_left = data;
    });
      this.clientNum = "";
      this.clientMsg = "";
  };

});

})();
